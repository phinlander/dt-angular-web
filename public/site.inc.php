<?php
  require_once(__DIR__."/../vendor/autoload.php");
  DTSettingsConfig::initShared(__DIR__."/../../local/config.json");
  DTSettingsAPIs::initShared(__DIR__."/../../local/apis.json");
  $dt_token = DTAPI::consumerTokenForAPI("dt-angular-api");