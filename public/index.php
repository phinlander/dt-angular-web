<?php
  require_once(__DIR__.'/site.inc.php');

?>

<!DOCTYPE html>
<html lang='en-US'>
  <head>
    <!-- Resolution fix -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>DT_AngularJS</title>

    <base href='/'>

    <meta name='Description' content='DT_AngularJS'>

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src='bower_components/jquery-ui/jquery-ui.js'></script>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
    <script src='bower_components/bootstrap/dist/js/bootstrap.js'></script>

    <!-- Angular -->
    <script src='bower_components/angular/angular.js'></script>
    <script src='bower_components/angular-resource/angular-resource.js'></script>
    <script src='bower_components/angular-bootstrap/ui-bootstrap.js'></script>
    <script src='bower_components/angular-bootstrap/ui-bootstrap-tpls.js'></script>
    <link rel='stylesheet' href='bower_components/angular-bootstrap/ui-bootstrap-csp.css'>
    <script src='bower_components/angular-touch/angular-touch.js'></script>
    <script src='bower_components/angular-animate/angular-animate.js'></script>
    <script src='bower_components/angular-ui-router/release/angular-ui-router.js'></script>
    <script src='bower_components/angular-sanitize/angular-sanitize.js'></script>

    <!-- The App -->
    <script src="js/app.js"></script>
    <script src='js/controllers.js'></script>
    <script src='js/services.js'></script>

    <!-- Styles -->
    <!--
    <link rel='stylesheet' href='css/style.css'>
    -->

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300,300italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,300,300italic,400italic,600,600italic,700italic,800italic' rel='stylesheet' type='text/css'>
  </head>
  <body ng-app='dtApp'>

    <div ui-view></div>

  </body>
</html>

