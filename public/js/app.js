'use strict';

var dtApp = angular.module(['dtApp'], [
  'dtControllers',
  'dtServices',
  'ui.router',
  'ngAnimate',
  'ngSanitize',
  'ngTouch',
  'ui.bootstrap'
])
.run([
  '$q', '$rootScope','$state','$stateParams',
  function($q, $rootScope, $state, $stateParams) {
    // Set defaults
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
  } 
])
.config([
  '$stateProvider','$urlRouterProvider','$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider
    .otherwise('/');

    $stateProvider
    .state('body', {
      abstract: true,
      url: '',
      templateUrl: 'partials/body.html',
      controller: 'NavController'
    })
    .state('index', {
      url: '/',
      templateUrl: 'partials/index.html',
      parent: 'body'
    })
    .state('animal', {
      abstract: true,
      url: '/animals',
      templateUrl: 'partials/animal.html',
      parent: 'body'
    })
    .state('animal.view', {
      url: '/:id',
      templateUrl: 'partials/animal.view.partial.html',
      controller: 'AnimalViewController'
    })
    .state('animal.list', {
      url: '',
      templateUrl: 'partials/animal.list.partial.html',
      controller: 'AnimalListController'
    })
    .state('planet', {
      abstract: true,
      url: '/planets',
      templateUrl: 'partials/planet.html',
      parent: 'body'
    })
    .state('planet.view', {
      url: '/:id',
      templateUrl: 'partials/planet.view.partial.html',
      controller: 'PlanetViewController'
    })
    .state('planet.list', {
      url: '',
      templateUrl: 'partials/planet.list.partial.html',
      controller: 'PlanetListController'
    })
    ;

    $locationProvider.html5Mode(true);
  }
]);