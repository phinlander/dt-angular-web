'use strict';

angular.module('dtServices', ['ngResource'])
.factory('Animal', function($resource) {
  return $resource('/api/animals/:id', { }, { });
})
.factory('Planet', function($resource) {
  return $resource('/api/planets/:id', { }, { });
})