'use strict';

var dtAppControllers = angular.module('dtControllers',['dtServices']);

/***
 * Nav Controller
 */

dtAppControllers.controller("NavController", function($rootScope, $state, $scope) {
  $scope.isActive = function(page) {
    return $state.is(page) ? 'active' : '';
  }
});

/***
 * Animal Controllers
 */
dtAppControllers.controller('AnimalViewController', function($scope, $stateParams, Animal) {
  var query = Animal.get({id: $stateParams.id}, function(animal) {
    $scope.animal = animal;
  });
});

dtAppControllers.controller('AnimalListController', function($scope, Animal) {
  var query = Animal.get({}, function(animals) {
    $scope.animals = animals.objects;
  });
});

/***
 * Planet Controllers
 */
dtAppControllers.controller('PlanetViewController', function($scope, $stateParams, Planet) {
  var query = Planet.get({id: $stateParams.id}, function(planet) {
    $scope.planet = planet;
    console.log(planet);
  });
});

dtAppControllers.controller('PlanetListController', function($scope, Planet) {
  var query = Planet.get({}, function(planets) {
    $scope.planets = planets.objects;
  });
});