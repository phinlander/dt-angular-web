<?php
  require_once(__DIR__.'/../site.inc.php');
  $consumer = new DTRESTProviderConsumer("dt-angular-api","animals.php",$dt_token);
  $response = $consumer->get($_REQUEST);

  if(!empty($_REQUEST))
    echo json_encode($response);
  else
    echo json_encode(array("objects" => $response));