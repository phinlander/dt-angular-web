# Setup required for dt-angular-web
---
## How to Run
```sh
mkdir -p dt-angular
cd dt-angular

### Choose one ###
#git clone https://gitlab.com/phinlander/dt-angular-web.git
#git clone git@gitlab.com:phinlander/dt-angular-web.git

./dt-angular-web/docker/init.sh dt-angular-web <port>

cd dt-angular-web
# Install bower
./bower.sh

cd ../local
# Edit apis.json to use <port>

### TEMPORARY ###
# Grab any updates that were made to make this example work while they get merged in
cd ../dt-angular-web
cp -r updates/* vendor/
cd ../
./docker/composer dt-angular-web install
```

### htaccess
This needs to be left in public for REST reasons

### Bower
Make sure you've installed bower on your machine then run bower.sh from the project's root directory (the same level as this README is in)

### Local Files
Make sure and copy these over. There should be a similar set in dt-angular-api as well. As long as apis.json match up, you're good to go.

These may have to have their IPs and Ports changed

### Apache
You'll need to pull dt-angular-web.conf into the root deepthought's apache and then restart the server. Using the run scripts should do the trick.

### WARNING
There may be modified DTConsumer* code in here from development that is not yet live. If everything breaks, it's because these changes weren't pulled in yet.