<?php
/**
 * DTRESTProviderConsumer
 *
 * Copyright (c) 2013-2014, Expressive Analytics, LLC <info@expressiveanalytics.com>.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @package    Deep Thought (Consumer)
 * @author     Blake Anderson <blake@expressiveanalytics.com>
 * @copyright  2013-2014 Expressive Analytics, LLC <info@expressiveanalytics.com>
 * @license    http://choosealicense.com/licenses/mit
 * @link       http://www.expressiveanalytics.com/
 * @since      version 1.0.0
 */

class DTRESTProviderConsumer extends DTProviderConsumer{
  
  function __construct($api_name=null,$path="",$token=null){
    parent::__construct($api_name,$path,$token);
  }

  /** primary method of making a request to a DTRESTProvider */
  // Removing anything referencing act
  public function request($method=null, array $params=array()){
    if(!isset($method))
      $method = isset($method)?$method:$this->default_method;
    DTSession::sharedSession(); //have to start the session
    $url = $this->url;
    $this->preprocessParams($params);
    // this cookie parameter is essential for getting the same session with each request (whether this *should* be done for public APIs is another question...)
    $r = DTHTTPRequest::makeHTTPRequest($url,$params,$method,$_SESSION["{$this->api["name"]}_cookies"]); //using $_SESSION directly because of reference
    return $this->handleStatusCodes($params,$r);
  }

  public function get(array $params=array()) {
    return $this->request("GET", $params);
  }

  public function post(array $params=array()) {
    return $this->request("POST", $params);
  }

  public function put(array $params=array()) {
    return $this->request("PUT", $params);
  }

  public function delete(array $params=array()) {
    return $this->request("DELETE", $params);
  }
  
  // Don't need act anymore
  protected function preprocessParams(array &$params=array()){
    $params["tok"] = $this->upgradeToken(isset($params["tok"])?$params["tok"]:$this->sync_token);
    if(!isset($params["tok"]))
      throw new Exception("Missing required request parameters (tok).");
  }

}