#!/bin/bash

docker run -d \
  -h $1 \
  --name $1 \
  -v $(PWD):/var/www/deepthought \
  -p $2:80 \
  expressiveanalytics/dt-development.php
docker exec -it $1 a2enmod rewrite
docker exec -it $1 service apache2 graceful
docker stop $1 && docker start $1
./docker/composer.sh $1 install