#!/bin/sh

# Copy docker run scripts (like this one)
mkdir -p docker
cp -i $1/docker/* docker/

# Grab data, if it exists
if [ -d "$1/data" ]; then
  mkdir -p data
  cp -i $1/data/* data/
fi

# Grab local configs, if they exist
if [ -d "$1/local" ]; then
  mkdir -p local
  cp -i $1/local/* local/
fi

# Grab scripts, if they exist
if [ -d "$1/scripts" ]; then
  mkdir -p scripts
  cp -i $1/scripts/* scripts/
fi

# Grab storage, if it exists
if [ -d "$1/storage" ]; then
  mkdir -p storage
  cp -i $1/storage/* storage/
fi

# Run docker to build the rest of the container
./docker/run.sh $1 $2

# Copy over the apache files and have a graceful restart
if [ -d "$1/apache" ]; then
  cp -i $1/apache/$1.conf apache/
  docker exec -it $1 service apache2 graceful
fi